/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

Class
    Foam::materialDatabase

Description
    Provides material properties of a gas.

\*---------------------------------------------------------------------------*/

{  
    IOdictionary phaseProperties
    (
        IOobject
        (
            "phaseProperties",
            mesh_.time().constant(),
            mesh_,
            IOobject::MUST_READ,
            IOobject::NO_WRITE
        )
    );

    // Diffusivity: whole  zone
    // ---------------------------
    label znId = mesh_.cellZones().findZoneID(zName_);
    const labelList& cells = mesh_.cellZones()[znId];
    const dictionary& diffDict = phaseProperties.subDict("diffusivity");

    Info<< nl << "Creating " << zName_ << "(zone " << znId << ") diffusivity models ";
    if (znId < 0){Info << "Warning!";}
    Info<< endl;

    diffModels_.set
    (
        0,
        new autoPtr<diffusivityModels::diffusivityModel>
        (
            diffusivityModels::diffusivityModel::New
                (mesh_, diff_, cells, diffDict)
        )
    );

    //  porous zones
    forAll(porousZones_, iz)
    {
        label znId =
            mesh_.cellZones().findZoneID(porousZones_[iz].zoneName());

        Info<< "    " << zName_ << "Zone " << znId
            << ": " << porousZones_[iz].zoneName() << endl;
        Info<< "        size = " << mesh_.cellZones()[znId].size() << nl;
        Info<< "    ";

        znCells_.set
        (
            iz,
            new labelList(mesh_.cellZones()[znId])
        );

        znDiffDict_.set
        (
            iz,
            new dictionary(porousZones_[iz].dict().subDict("diffusivity"))
        );

        diffModels_.set
        (
            iz + 1,
            new autoPtr<diffusivityModels::diffusivityModel>
            (
                diffusivityModels::diffusivityModel::New
                    (mesh_, diff_, znCells_[iz], znDiffDict_[iz])
            )
        );
    }
    Info << endl;
}

// ************************************************************************* //

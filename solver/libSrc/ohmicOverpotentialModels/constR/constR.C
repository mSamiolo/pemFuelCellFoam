/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

Class
    Foam::ohmicOverpotentialModel

Description
    Constant area specific resistance (ASR) - read from file.

\*---------------------------------------------------------------------------*/

#include "constR.H"
#include "addToRunTimeSelectionTable.H"
#include "volFields.H"


// * * * * * * * * * * * * * * Static Data Members * * * * * * * * * * * * * //

namespace Foam
{
    defineTypeNameAndDebug(constR, 0);
    addToRunTimeSelectionTable(ohmicOverpotentialModel, constR, dictionary);
}


// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::constR::constR
(
    const fvMesh& mesh,
    const patchDatabase& pm,
    const dictionary& dict
)
:
    ohmicOverpotentialModel(mesh, pm, dict)
{
    Info<< "Constant ohmic area specific resistance: " << R() << endl;
    if (R().value() <= 0)
    {
	Info << "Warning: Did you provide an ASR value?" << endl;
    }
}


// * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * * //

Foam::tmp<Foam::scalarField> Foam::constR::ASR
(
    const scalarField& anodeT,
    const scalarField& lambda
) const
{
    tmp<scalarField> pASR
    (
        new scalarField (anodeT.size(), R().value())
    );

    return pASR;
}

// ************************************************************************* //

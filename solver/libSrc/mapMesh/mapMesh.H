/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

Class
    Foam::mapMesh

Description
    Provides an interpolation between child and parent mesh.

\*---------------------------------------------------------------------------*/

#ifndef mapMesh_H
#define mapMesh_H


#include "fvMesh.H"
#include "labelIOList.H"
#include "scalarField.H"
#include "Time.H"
#include "volFields.H"
#include "surfaceMesh.H"
#include "fvsPatchField.H"
#include "patchDatabase.H"

namespace Foam
{

/*---------------------------------------------------------------------------*\
                       Class mapMesh Declaration
\*---------------------------------------------------------------------------*/

class mapMesh
{
    // Private data

        const fvMesh& mesh_;
        //- Provides names and IDs of patches
        patchDatabase& pm_;

        //- Addressing information of the faces
        const labelIOList faceRegionAddressing_;
        //- Mapping information for the faces
        labelList faceMap_;
        //- Direction of the faces between child and parent mesh
        // (1=the same; -1=inverse)
        scalarField faceMask_;
        //- Mapping information for cell values
        const labelIOList cellMap_;
        //- Mapping information for patches
        const labelIOList patchesMap_;


    // Private member functions

        //- Disallow copy construct
        mapMesh(const mapMesh&) = delete;

        //- Disallow default bitwise assignment
        void operator=(const mapMesh&) = delete;


public:

    // Constructors

        mapMesh(const fvMesh&, patchDatabase&);


    // Destructor

        ~mapMesh(){};


    // Member Functions

	//- Return cell addressing between parent and child mesh
        const labelList& cellMap()
        {
            return cellMap_;
        }
	
	//- Return face addressing between parent and child mesh
        labelList& faceMap()
        {
            return faceMap_;
        }

	//- Map field from child to parent mesh
        void rmap(volScalarField&, const volScalarField&);
        //- Map scalar from child to parent mesh
        void rmap(volScalarField&, const dimensionedScalar&);
        //- Map field from child to parent mesh
        void rmap(volScalarField&, scalarField&);
        //- Map from child to parent mesh, including patches
        void rmap(surfaceScalarField&, const surfaceScalarField&);
	//- Set field to zero
        void zeroInternalFlux(surfaceScalarField&);
};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //

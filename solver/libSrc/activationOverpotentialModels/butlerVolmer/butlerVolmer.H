/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

Class
    Foam::butlerVolmer

Description
    Solves the Butler-Volmer equation as
    i = i0[exp(2*alpha*F*eta/RT-exp(-2*(1-alpha)*F*eta/RT)]

    The exchange current density is given as
    i0 = gamma*(1-s)*(p/pRef)^stoichiometricNumber*exp(-E/(RT)*(1-T/Tref))
    eta = activation overpotential obtained by numerical inversion of BV eqn.
    i = local current density
    i0 = exchange current density
    alpha = transfer coefficient 
    R = universal gas constant
    F = Faraday constant
    gamma = exchange current density at standard condition

\*---------------------------------------------------------------------------*/

#ifndef butlerVolmer_H
#define butlerVolmer_H

#include "activationOverpotentialModel.H"
#include "dimensionedScalar.H"
#include "primitiveFieldsFwd.H"
#include "labelList.H"
#include "volFieldsFwd.H"
#include "fvMesh.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

/*---------------------------------------------------------------------------*\
                           Class butlerVolmer Declaration
\*---------------------------------------------------------------------------*/

class butlerVolmer
:
    public activationOverpotentialModel
{
    // Private member functions

        //- Disallow copy construct
        butlerVolmer(const butlerVolmer&) = delete;

        //- Disallow default bitwise assignment
        void operator=(const butlerVolmer&) = delete;


public:

    //- Runtime type information
    TypeName("butlerVolmer");


    // Constructors

        butlerVolmer
        (
            const speciesTable& speciesNames,
            const dictionary& dict
        );


    // Destructor

        ~butlerVolmer()
        {}


    // Member functions

	//- Returns the exchange current density
        virtual const tmp<scalarField> exchangeCurrentDensity
        (
            const scalarField& electrodeT,
            const scalarField& pPatch,
            const PtrList<scalarField> molFraction
        ) const;

	//- Returns the current density as a function of overpotential
        virtual tmp<scalarField> currentDensity
        (
            const scalarField& electrodeT,
            const scalarField& pPatch,
            const PtrList<scalarField> molFraction,
            const scalarField& overPotential
        ) const;

	//- Returns the local current density
        scalar localCurrentDensity(scalar i0, scalar T, scalar eta) const;

	//- Returns the overpotential as a function of current density
        virtual tmp<scalarField> overPotential
        (
            const scalarField& electrodeT,
            const scalarField& pPatch,
            const PtrList<scalarField> molFraction,
            const scalarField& waterBlockage,
            const scalarField& currentDensity
        ) const;
};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //


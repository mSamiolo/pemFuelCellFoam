/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

Class
    Foam::noActivation

Description
    No activation overpotential.

\*---------------------------------------------------------------------------*/

#include "noActivation.H"
#include "addToRunTimeSelectionTable.H"
#include "volFields.H"

// * * * * * * * * * * * * * * Static Data Members * * * * * * * * * * * * * //

namespace Foam
{
    defineTypeNameAndDebug(noActivation, 0);
    addToRunTimeSelectionTable(activationOverpotentialModel, noActivation, dictionary);
}


// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::noActivation::noActivation
(
    const speciesTable& speciesNames,
    const dictionary& dict
)
:
    activationOverpotentialModel(speciesNames, dict)
{}


// * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * * //

const Foam::tmp<Foam::scalarField> Foam::noActivation::exchangeCurrentDensity
(
    const scalarField& electrodeT,
    const scalarField& pPatch,
    const PtrList<scalarField> molFraction
) const
{
    tmp<scalarField> pexchangeCurrentDensity
    (
        new scalarField (electrodeT.size(), 1)
    );
    scalarField& exCurrentDensity = pexchangeCurrentDensity.ref();

    exCurrentDensity = 1e16;

    return pexchangeCurrentDensity;
}


Foam::tmp<Foam::scalarField> Foam::noActivation::currentDensity
(
    const scalarField& electrodeT,
    const scalarField& pPatch,
    const PtrList<scalarField> molFraction,
    const scalarField& overPotential
) const
{
    tmp<scalarField> pcurrentDensity
    (
        new scalarField (electrodeT.size(), 1)
    );

    scalarField& cCurrentDensity = pcurrentDensity.ref();

    forAll(electrodeT, cellI)
    {
        cCurrentDensity[cellI] = 1e16;
    }

    return pcurrentDensity;
}


Foam::tmp<Foam::scalarField> Foam::noActivation::overPotential
(
    const scalarField& electrodeT,
    const scalarField& pPatch,
    const PtrList<scalarField> molFraction,
    const scalarField& waterBlockage,
    const scalarField& currentDensity
) const
{
    tmp<scalarField> pOverPotential
    (
        new scalarField(electrodeT.size(), 0.0)
    );

    scalarField& cOverPotential = pOverPotential.ref();

    cOverPotential = 0;

    return pOverPotential;
}

// ************************************************************************* //

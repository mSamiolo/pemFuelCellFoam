/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

Class
    Foam::simpleTafel

\*---------------------------------------------------------------------------*/

#include "simpleTafel.H"

#include "addToRunTimeSelectionTable.H"
#include "volFields.H"

// * * * * * * * * * * * * * * Static Data Members * * * * * * * * * * * * * //

namespace Foam
{
    defineTypeNameAndDebug(simpleTafel, 0);
    addToRunTimeSelectionTable(activationOverpotentialModel, simpleTafel, dictionary);
}


// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::simpleTafel::simpleTafel
(
    const speciesTable& speciesNames,
    const dictionary& dict
)
:
    activationOverpotentialModel(speciesNames, dict)
{
    if(gamma().value() < 0) // exchange current density
    {
        FatalError << "Specify gamma" << abort(FatalError);
    }
    if(b().value() < 0) // Tafel slope
    {
        FatalError << "Specify b" << abort(FatalError);
    }
    if(Tref().value() < 0) // reference temperature
    {
        FatalError << "Specify Tref" << abort(FatalError);
    }
}


// * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * * //

const Foam::tmp<Foam::scalarField> Foam::simpleTafel::exchangeCurrentDensity
(
    const scalarField& electrodeT,
    const scalarField& pPatch,
    const PtrList<scalarField> molFraction
) const
{
    tmp<scalarField> pexchangeCurrentDensity
    (
        new scalarField (electrodeT.size(), 1)
    );
    scalarField& exCurrentDensity = pexchangeCurrentDensity.ref();

    // Compute local exchange current density
    forAll(stoichiometricNumbers(), fsi)
    {
        if(activeSwitch()[fsi])
        {
            exCurrentDensity *= Foam::pow
            (
                (pPatch*molFraction[fsi]/physicalConstant::pAtm.value()),
                stoichiometricNumbers()[fsi]
            );
        }
    }

    exCurrentDensity *= gamma().value();
    exCurrentDensity *= Foam::exp
    (
        -1.*E().value()/(physicalConstant::Rgas.value()*electrodeT) * (1-electrodeT/Tref().value())
    );

    return pexchangeCurrentDensity;
}

Foam::scalar Foam::simpleTafel::localCurrentDensity
(
    scalar i0,
    scalar T,
    scalar eta
) const
{
    return i0*Foam::exp(eta/c().value());
}


Foam::tmp<Foam::scalarField> Foam::simpleTafel::currentDensity
(
    const scalarField& electrodeT,
    const scalarField& pPatch,
    const PtrList<scalarField> molFraction,
    const scalarField& overPotential
) const
{
    tmp<scalarField> pcurrentDensity
    (
        new scalarField (electrodeT.size(), 1)
    );

    scalarField& cCurrentDensity = pcurrentDensity.ref();

    scalarField i0 = exchangeCurrentDensity(electrodeT, pPatch, molFraction).ref();

    forAll(electrodeT, cellI)
    {
        cCurrentDensity[cellI] = localCurrentDensity(i0[cellI], electrodeT[cellI], overPotential[cellI]);
    }

    return pcurrentDensity;
}


Foam::tmp<Foam::scalarField> Foam::simpleTafel::overPotential
(
    const scalarField& electrodeT,
    const scalarField& pPatch,
    const PtrList<scalarField> molFraction,
    const scalarField& waterBlockage,
    const scalarField& currentDensity
) const
{
    tmp<scalarField> pOverPotential
    (
        new scalarField(electrodeT.size(), 0.0)
    );

    scalarField& cOverPotential = pOverPotential.ref();

    scalarField i0(waterBlockage*exchangeCurrentDensity(electrodeT, pPatch, molFraction).ref());
    
    scalarField quotient = currentDensity/i0;

    // should not get zero because of log
    check("j / j0", quotient, SMALL, GREAT);
    quotient = Foam::max(SMALL, quotient);
    cOverPotential = Foam::max(0.0, Foam::log(quotient)); 

    // natural Tafel slope (not base 10), because Foam::log is natural logarithm (ln)
    cOverPotential *= c().value();

    return pOverPotential;
}

// ************************************************************************* //

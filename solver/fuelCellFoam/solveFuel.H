{
    Info << nl << "Solving fuel flow" << endl;

    pdFuel.storePrevIter();

    volScalarField eFuel(matFuel.epsilon0()*(1.0 - sFuel));
    surfaceScalarField eFuelf(fvc::interpolate(eFuel));

    fvVectorMatrix UEqn
    (
        fvm::div(eFuelf*phiFuel, Ufuel, "div(phi,U)")
      - fvm::laplacian(eFuel*matFuel.mu(), Ufuel, "laplacian(mu,U)")
    );

    matFuel.porousZones().addResistance(eFuel, Foam::pow(1. - sFuel, 4), UEqn);

    UEqn.relax();

    if(pimple.momentumPredictor())
    {
        solve
        (
            UEqn
          ==
            fvc::reconstruct
            (
     	        eFuelf*
                (
                  - ghfFuel*fvc::snGrad(matFuel.rho())
                  - fvc::snGrad(pdFuel)
                )*fuelMesh.magSf()
            )
        );
    }

    while (pimple.correct())
    {
        volScalarField rAU("rAU",1.0/UEqn.A());
        surfaceScalarField rhorAUf("rhorAUf", fvc::interpolate(matFuel.rho()*rAU));
        volVectorField HbyA(constrainHbyA(rAU*UEqn.H(), Ufuel, pdFuel));

        surfaceScalarField phig(-eFuelf*rhorAUf*ghfFuel*fvc::snGrad(matFuel.rho())*fuelMesh.magSf());

        surfaceScalarField phiHbyA
        (
            "phiHbyA",
	    fvc::flux(matFuel.rho()*HbyA)
        );

        adjustPhi(phiHbyA, Ufuel, pdFuel);

        phiHbyA += phig;

        constrainPressure(pdFuel, matFuel.rho(), Ufuel, phiHbyA, rhorAUf);

        while (pimple.correctNonOrthogonal())
        {
            fvScalarMatrix pdEqn
            (
                fvm::laplacian(eFuelf*eFuelf*rhorAUf, pdFuel, "laplacian(rhorAUf,pd)") 
             == fvc::div(eFuelf*phiHbyA) + phaseChangeFuel.Svl()
            );

            pdEqn.setReference(pFuelRefCell, pFuelRefValue);
            pdEqn.solve();

            if (pimple.finalNonOrthogonalIter())
            {
                phiFuel = phiHbyA - eFuelf*pdEqn.flux();

                pdFuel.relax();

                Ufuel = HbyA + rAU*fvc::reconstruct((phig - eFuelf*pdEqn.flux())/rhorAUf);
                Ufuel.correctBoundaryConditions();
            }
        }
        pFuel = pdFuel + matFuel.rho()*ghFuel;

        continuityErrs(phiFuel);

        if (pdFuel.needReference())
        {
            pFuel += dimensionedScalar
            (
                "p",
                pFuel.dimensions(),
                pFuelRefValue - getRefCellValue(pFuel, pFuelRefCell)
            );
            pdFuel = pFuel - matFuel.rho()*ghFuel;
        }
    }
    lim.check(Ufuel, -10, 10);
    lim.check(pdFuel, 0, 3e6);
}

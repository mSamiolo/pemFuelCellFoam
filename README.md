# fuelCellFoam

Solver written for OpenFOAM-v2112, to explore PEMFC devices.

## Compilation

On a precompiled version of OpenFOAM, copy the ```solver``` source code into the openFOAM-v2112 application directory.

```sh
mkdir $WM_PROJECT_DIR/applications/solvers/electrochemistry
cp -r solver/* $WM_PROJECT_DIR/applications/solvers/electrochemistry
```

And start the compilation of the application

```sh
cd $WM_PROJECT_DIR/applications/solvers/electrochemistry
./Allwmake
```

## Test

Check out the case file attached to the project:

```sh
cp -r testcase/juelich .
cd juelich
make
```